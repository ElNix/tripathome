# TripAtHome #

TripAtHome is a time management web game. The player can farm, mine and improve his equipment.
After earning ressources, the player is rewarded with virtual trips in the most beautiful cities in the world (pictures dynamically extracted from Flick's API).

### Team ###

- Nicolas BENOIT : architect, developer and designer
- Paulin NOVOTNY : graphics

### Configuration ###

- Needs a WAMP or LAMP environment
- A MySQL database for saves

### Contribution guidelines ###

- Don't hesitate to ask Nicolas - ElNix - Benoit at nico.benoit@gmail.com if you want to contribute !